import AuiFlag, { FlagGroup } from '@atlaskit/flag';
import React, { Component } from 'react';
import { SuccessIcon } from '@atlaskit/icon';

const FLAGID_PREFIX = 'ap-flag-';

class Flag extends React.Component {

  render () {
    return (
        <AuiFlag
        description={this.props.options.body}
        icon={null}
        id={"derp"}
        title={this.props.options.title}
      />);
  }

}

export default Flag;